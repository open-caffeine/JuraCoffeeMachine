/**
 * Copyright (c) 2021-2022 Alexander Fuchs <alex.fu27@gmail.com> and the
 * contributors of the open-caffeine project.
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 *
 * We are not affiliated, associated, authorized, endorsed by, or in any way
 * officially connected with Jura Elektroapparate AG. JURA and the JURA logo are
 * trademarks or registered trademarks of Jura Elektroapparate AG in Switzerland
 * and/or other countries.
 *
 * Using our software or hardware with you coffee machine may void your warranty
 * and we cannot be held liable for any damage or operating failure.
 */

#include <cstring>
#include <cstdlib>
#include <JuraInterface.h>
#include "JuraCoffeeMachine.h"
#include "Product.h"

#ifdef ARDUINO
#include <Arduino.h>

#elif defined(ESP_PLATFORM)
#include <esp_timer.h>

unsigned long millis()
{
	return esp_timer_get_time() / 1000UL;
}

#endif

JuraCoffeeMachine::JuraCoffeeMachine(JuraInterface& interface):
	interface(interface), handshakeState(OFF), debugCallback(nullptr)
{}

void JuraCoffeeMachine::startHandshake(bool doObfuscatedHandshake)
{
	isHandshakeObfuscated = doObfuscatedHandshake;
	if (isHandshakeObfuscated) {
		interface.sendObfuscated("@T1");
		handshakeState = AWAIT_T1;
	} else {
		handshakeState = STARTED;
	}
}

bool JuraCoffeeMachine::isHandshakeDone() const
{
	return handshakeState == DONE;
}

unsigned long JuraCoffeeMachine::getFlags() const
{
	return flags;
}

void JuraCoffeeMachine::startProduction(const Product& product) 
{
	StringBuffer sb(48);
	sb.append("@TP:");
	product.format(sb);
	interface.sendObfuscated(sb.get());
}

void JuraCoffeeMachine::update()
{
	interface.update();
	if (interface.messageAvailable()) {
		JuraMessageView msg(interface.getMessage());

		if (handshakeState != OFF && handshakeState != DONE) {
			updateHandshake(msg);
		}

		updateFlags(msg);

		if (debugCallback) {
			debugCallback(msg);
		}
	}
	if (handshakeState != OFF && handshakeState != DONE) {
		updateHandshake();
	}
}

void JuraCoffeeMachine::sendRaw(const char* message, bool obfuscated)
{
	interface.send(message, obfuscated);
}

void JuraCoffeeMachine::attachDebug(JuraCoffeeMachine::DebugCallback cb)
{
	debugCallback = cb;
}


void JuraCoffeeMachine::updateFlags(const JuraMessageView& msg)
{
	if (strcmp(msg.getCommand(), "TF") != 0) {
		return;
	}
	if (!msg.hasFirstParameter()) {
		return;
	}
	const char* flagstr = msg.getFirstParameter();
	flags = strtoul(flagstr, nullptr, 16);
}

void JuraCoffeeMachine::updateHandshake()
{
	if (handshakeState == STARTED) {
		interface.sendPlain("TY:");
		handshakeState = TYPE_REQUESTED;
		handshakeStartTime = millis();
		return;
	}
	if (handshakeState == TYPE_REQUESTED) {
		if ((handshakeStartTime + 1500) < millis()) {
			// machine took too long to answer
			handshakeState = STARTED;
		}
	}
}

void JuraCoffeeMachine::updateHandshake(const JuraMessageView& msg)
{
	if (handshakeState == TYPE_REQUESTED) {
		if (strcmp(msg.getCommand(), "ty") == 0) {
			// TODO save type
			interface.send("@T1", isHandshakeObfuscated);
			handshakeState = AWAIT_T1;
		}
		return;
	}
	if (handshakeState == AWAIT_T1) {
		if (strcmp(msg.getCommand(), "t1") == 0) {
			handshakeState = AWAIT_T2;
		}
		return;
	}
	if (handshakeState == AWAIT_T2) {
		if (strcmp(msg.getCommand(), "T2") == 0) {
			interface.send("@t2:81", isHandshakeObfuscated);
			handshakeState = AWAIT_T3;
		}
		return;
	}
	if (handshakeState == AWAIT_T3) {
		if (strcmp(msg.getCommand(), "T3") == 0) {
			// TODO save type no
			interface.send("@t3", isHandshakeObfuscated);
			handshakeState = AWAIT_FLAGS;
		}
		return;
	}
	if (handshakeState == AWAIT_FLAGS) {
		if (strcmp(msg.getCommand(), "TF") == 0) {
			// TODO update flags
			// TODO normally, blueFrog will now request @TR:37, but I don't know
			// what that does so I wont
			// If idle, BlueFrog will also mute the machine by sending @T0
			handshakeState = DONE;
		}
	}
}


