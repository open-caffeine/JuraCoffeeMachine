/**
 * Copyright (c) 2021-2022 Alexander Fuchs <alex.fu27@gmail.com> and the
 * contributors of the open-caffeine project.
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 *
 * We are not affiliated, associated, authorized, endorsed by, or in any way
 * officially connected with Jura Elektroapparate AG. JURA and the JURA logo are
 * trademarks or registered trademarks of Jura Elektroapparate AG in Switzerland
 * and/or other countries.
 *
 * Using our software or hardware with you coffee machine may void your warranty
 * and we cannot be held liable for any damage or operating failure.
 */

#include "Product.h"
#include <StringBuffer.h>
#include <stdlib.h>

Product Product::fromProductCommandParameter(const char* par)
{
	uint8_t vals[14];
	char xx[3];
	for (int i = 0; i < 14; ++i) {
		xx[0] = par[2 * i];
		xx[1] = par[2 * i + 1];
		vals[i] = strtoul(xx, nullptr, 16);
	}
	return Product(vals);
}

Product::Product(const uint8_t vals[14]):
	productCode(vals[0]),
	finenessOfGrinding(vals[1]),
	strength(vals[2]),
	coffeeAmount(vals[3]),
	milkAmount(vals[4]),
	foamAmount(vals[5]),
	temperature(vals[6]),
	strokes(vals[7]),
	f09(vals[8]),
	bypassAmount(vals[9]),
	milkPause(vals[10]),
	f12(vals[11]),
	f13(vals[12]),
	temperatures(vals[13])
{}

void Product::format(StringBuffer& buf) const
{
	buf.appendHex(productCode, 1);
	buf.appendHex(finenessOfGrinding, 1);
	buf.appendHex(strength, 1);
	buf.appendHex(coffeeAmount, 1);
	buf.appendHex(milkAmount, 1);
	buf.appendHex(foamAmount, 1);
	buf.appendHex(temperature, 1);
	buf.appendHex(strokes, 1);
	buf.appendHex(f09, 1);
	buf.appendHex(bypassAmount, 1);
	buf.appendHex(milkPause, 1);
	buf.appendHex(f12, 1);
	buf.appendHex(f13, 1);
	buf.appendHex(temperatures, 1);
}

const Product Product::Default{0x03, 0x00, 0x04, 0x14};

