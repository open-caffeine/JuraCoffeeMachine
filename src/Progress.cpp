/**
 * Copyright (c) 2021-2022 Alexander Fuchs <alex.fu27@gmail.com> and the
 * contributors of the open-caffeine project.
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 *
 * We are not affiliated, associated, authorized, endorsed by, or in any way
 * officially connected with Jura Elektroapparate AG. JURA and the JURA logo are
 * trademarks or registered trademarks of Jura Elektroapparate AG in Switzerland
 * and/or other countries.
 *
 * Using our software or hardware with you coffee machine may void your warranty
 * and we cannot be held liable for any damage or operating failure.
 */

#include "Progress.h"
#include "Product.h"
#include <StringBuffer.h>
#include <stddef.h>

void Progress::format(StringBuffer& sb) const
{
	sb.appendHex(progressCode, 1);
}

BrewingProgress::BrewingProgress(const Product& product):
	productCode(product.productCode),
	strength(product.strength),
	strengthAgain(product.strength),
	amountCurrent(0),
	amountTarget(product.coffeeAmount),
	f5(0xff),
	f6(0xff),
	f7(0xff),
	f8(0xff),
	temperatures(product.temperature * 0x11),
	f10(0xff),
	f11(0xff),
	f12(0x00)
{}

void BrewingProgress::format(StringBuffer& sb) const
{
	Progress::format(sb);
	sb.appendHex(productCode, 1);
	sb.appendHex(strength, 1);
	sb.appendHex(strengthAgain, 1);
	sb.appendHex(amountCurrent, 1);
	sb.appendHex(amountTarget, 1);
	sb.appendHex(f5, 1);
	sb.appendHex(f6, 1);
	sb.appendHex(f7, 1);
	sb.appendHex(f8, 1);
	sb.appendHex(temperatures, 1);
	sb.appendHex(f10, 1);
	sb.appendHex(f11, 1);
	sb.appendHex(f12, 1);
}

EnjoyProgress::EnjoyProgress(uint8_t productCode):
	Progress(0x3E),
	productCode(productCode)
{}

EnjoyProgress::EnjoyProgress(const Product& product):
	EnjoyProgress(product.productCode)
{}

void EnjoyProgress::format(StringBuffer& sb) const
{
	Progress::format(sb);
	sb.appendHex(productCode, 1);
}


template<size_t a, size_t... more>
struct max {
	using other = max<more...>;
	constexpr static size_t val = (a < other::val ? other::val : a);
};

template <size_t a>
struct max<a> {
	constexpr static size_t val = a;
};

ProgressBuffer::ProgressBuffer():
	ptr((Progress*) ::operator new(max<sizeof(Progress), sizeof(BrewingProgress)>::val))
{
	new(ptr) Progress();
}

ProgressBuffer::~ProgressBuffer()
{
	ptr->~Progress();
	::operator delete(ptr);
}

ProgressBuffer::operator BrewingProgress*()
{
	if (((Progress*) ptr)->getType() != Progress::Type::BREWING) {
		return nullptr;
	}
	return (BrewingProgress*) ptr;
}

