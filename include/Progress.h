/**
 * Copyright (c) 2021-2022 Alexander Fuchs <alex.fu27@gmail.com> and the
 * contributors of the open-caffeine project.
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 *
 * We are not affiliated, associated, authorized, endorsed by, or in any way
 * officially connected with Jura Elektroapparate AG. JURA and the JURA logo are
 * trademarks or registered trademarks of Jura Elektroapparate AG in Switzerland
 * and/or other countries.
 *
 * Using our software or hardware with you coffee machine may void your warranty
 * and we cannot be held liable for any damage or operating failure.
 */

#ifndef JURA_PROGRESS_H
#define JURA_PROGRESS_H

#include <stdint.h>

class StringBuffer;
class Product;

// placement new not defined in arduino
inline void* operator new (unsigned int, void* ptr)
{
	return ptr;
};

struct Progress
{
	enum class Type {
		BASE,
		BREWING
	};

	uint8_t progressCode;

	Progress() = default;
	Progress(uint8_t code): progressCode(code) {}
	virtual ~Progress() = default;

	virtual Type getType() const { return Type::BASE; }

	virtual void format(StringBuffer&) const;
};

struct BrewingProgress: public Progress
{
	/* ex: @TV:39030404001BFFFFFFFF11FFFF00
	 * 	   @TV:390301010014FFFFFFFF22FFFF00
	 */
	uint8_t productCode;
	uint8_t strength;
	uint8_t strengthAgain;
	uint8_t amountCurrent;
	uint8_t amountTarget;
	uint8_t f5;
	uint8_t f6;
	uint8_t f7;
	uint8_t f8;
	uint8_t temperatures;
	uint8_t f10;
	uint8_t f11;
	uint8_t f12;

	BrewingProgress():
		Progress(),
		f5(0xff),
		f6(0xff),
		f7(0xff),
		f8(0xff),
		temperatures(0x11),
		f10(0xff),
		f11(0xff)
	{}

	BrewingProgress(const Product&);
	virtual ~BrewingProgress() override = default;

	virtual Type getType() const { return Type::BREWING; }

	virtual void format(StringBuffer&) const override;
};

struct EnjoyProgress: public Progress
{
	uint8_t productCode;

	EnjoyProgress(uint8_t productCode);
	EnjoyProgress(const Product&);

	virtual void format(StringBuffer&) const override;
};

class ProgressBuffer
{
	/* A buffer that is always large enough to contain any object deriving from
	 * Progress.
	 */
public:
	class Reference {
	public:
		Reference(ProgressBuffer& b): b(b) {}

		template<typename T>
		T& operator =(T&& p)
		{
			Progress* x = &p; // assert that p is derived from Progress
			(void) x;
			((Progress*) b.ptr)->~Progress();
			new(b.ptr) T(p);
			return *(T*) b.ptr;
		}

		operator const Progress&() const
		{
			return *(Progress*) b.ptr;
		}


	private:
		ProgressBuffer& b;
	};

	ProgressBuffer();
	~ProgressBuffer();

	operator Progress*() { return (Progress*) ptr; }
	operator const Progress*() const { return (const Progress*) ptr; }
	explicit operator void*() { return ptr; }

	Reference operator *() { return Reference(*this); }

	/* returns nullptr if no BrewingProgress is contained. */
	explicit operator BrewingProgress*();

private:
	Progress* ptr;
};

#endif

