/**
 * Copyright (c) 2021-2022 Alexander Fuchs <alex.fu27@gmail.com> and the
 * contributors of the open-caffeine project.
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 *
 * We are not affiliated, associated, authorized, endorsed by, or in any way
 * officially connected with Jura Elektroapparate AG. JURA and the JURA logo are
 * trademarks or registered trademarks of Jura Elektroapparate AG in Switzerland
 * and/or other countries.
 *
 * Using our software or hardware with you coffee machine may void your warranty
 * and we cannot be held liable for any damage or operating failure.
 */

#ifndef JURA_PRODUCT_H
#define JURA_PRODUCT_H

#include <stdint.h>

class StringBuffer;

struct Product
{
	uint8_t productCode;
	uint8_t finenessOfGrinding;
	uint8_t strength;
	uint8_t coffeeAmount;
	uint8_t milkAmount;
	uint8_t foamAmount;
	uint8_t temperature;
	uint8_t strokes;
	uint8_t f09 = 0x01;
	uint8_t bypassAmount;
	uint8_t milkPause;
	uint8_t f12 = 0x00;
	uint8_t f13 = 0x00;
	uint8_t temperatures;

	static const Product Default;

	Product() = default;
	Product(const Product&) = default;
	Product(Product&&) = default;
	Product& operator =(Product&&) = default;
	Product& operator =(const Product&) = default;

	Product(uint8_t productCode, uint8_t finenessOfGrinding, uint8_t strength, uint8_t coffeeAmount):
		productCode(productCode), finenessOfGrinding(finenessOfGrinding), strength(strength), coffeeAmount(coffeeAmount)
	{}

	Product(const uint8_t vals[14]);

	void format(StringBuffer&) const;

	static Product fromProductCommandParameter(const char*);
};

#endif

