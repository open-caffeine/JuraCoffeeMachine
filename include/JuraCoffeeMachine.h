/**
 * Copyright (c) 2021-2022 Alexander Fuchs <alex.fu27@gmail.com> and the
 * contributors of the open-caffeine project.
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 *
 * We are not affiliated, associated, authorized, endorsed by, or in any way
 * officially connected with Jura Elektroapparate AG. JURA and the JURA logo are
 * trademarks or registered trademarks of Jura Elektroapparate AG in Switzerland
 * and/or other countries.
 *
 * Using our software or hardware with you coffee machine may void your warranty
 * and we cannot be held liable for any damage or operating failure.
 */

#ifndef JURA_COFFEE_MACHINE_H
#define JURA_COFFEE_MACHINE_H

class JuraInterface;
class JuraMessageView;

class Product;
class Process;

class JuraCoffeeMachine
{
public:
	using DebugCallback = void (*)(const JuraMessageView&);

	JuraCoffeeMachine(JuraInterface& interface);
	void startHandshake(bool doObfuscatedHandshake);
	bool isHandshakeDone() const;
	unsigned long getFlags() const;

	/* update will also update the underlying interface */
	void update();

	void startProduction(const Product&);

	void sendRaw(const char* message, bool obfuscated);

	/* attach a debug callback that will be called on every received message.
	 * to disable just pass nullptr
	 */
	void attachDebug(DebugCallback);

protected:
	enum HandshakeState {
		OFF,
		STARTED,
		TYPE_REQUESTED,
		AWAIT_T1,
		AWAIT_T2,
		AWAIT_T3,
		AWAIT_FLAGS,
		DONE
	};

private:
	JuraInterface& interface;
	HandshakeState handshakeState;
	unsigned long handshakeStartTime;
	unsigned long flags;
	bool isHandshakeObfuscated;
	DebugCallback debugCallback;

	void updateHandshake();
	void updateHandshake(const JuraMessageView&);
	void updateFlags(const JuraMessageView&);
};

#endif

